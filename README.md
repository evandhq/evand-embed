# evand-embed

**Evand-embed** offers embedding [Evand's](https://evand.ir) events into your webpage.

# Development

Clone this repo:

```
git@github.com:evandhq/evand-embed.git
```

Install dependencies with `npm`:

```
npm install
```

# Running development server

You can start webpack dev server by runnning the following command:

```
npm run dev
```

# Building

```
npm run build
```
