var persianNumber = require('persian').persianNumber;

module.exports = {
  soldOut: 'ظرفیت تمام شد',
  deadLine: 'مهلت ثبت نام',
  ticketTitle: 'عنوان بلیت',
  price: 'قیمت (تومان)',
  count: 'تعداد',
  evand: 'ایوند',
  row: 'ردیف',
  total: 'مجموع مبلغ:',
  invalidDiscount: 'کد تخفیف وارد شده صحیح نیست.',
  confirm: 'تایید',
  confirmAndPay: 'تایید و پرداخت',
  ticketHeading: function (i, attendee) {
    return 'بلیت شماره ' + persianNumber(i) + ' - ' + attendee.ticket.data.title;
  }
};
