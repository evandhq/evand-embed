var $ = require('mquery');
var logo = require('./images/logo.png');
var existingCopyright = $('#evand .evand-copyright');
var existingContent  = $('#evand #evand-content');

if (existingCopyright.items.length === 0) {
  var copyright = $
    .create('div')
    .addClass('evand-copyright')
    .html(
      '<a href="http://evand.ir" title="قدرت‌گرفته از ایوند"><img src="' + logo + '" alt="قدرت‌گرفته از ایوند"></a>' +
      '<iframe src="https://evand.ir/embed/' + evand.eventSlug + '" height="0" width="0" style="border: none;"></iframe>'
    )
    .css({
      fontSize: '.9em',
      textAlign: 'right',
      direction: 'ltr'
    });
}else{
  var copyright = existingCopyright;
}
if (existingContent.items.length === 0) {
  var wrapper = $.create('div');
}else{
  var wrapper = existingContent;
}




$('#evand')
  .append(wrapper)
  .append(copyright)
  .append(require('./views/loading').element)
  .append(require('./views/notification').element);

module.exports = wrapper;
