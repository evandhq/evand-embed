var config = {
   apiPath: 'https://api.evand.ir'
   // apiPath: 'https://alpha.evand.ir'
  // apiPath: 'http://feature.evand.dev'
};

config.captchaUrl = config.apiPath + '/captcha';
config.eventUrl = config.apiPath + '/events/' + evand.eventSlug + '?include=tickets';
config.orderUrl = config.apiPath + '/events/' + evand.eventSlug + '/orders';
config.discountCheckUrl = function (discountCode) {
  return config.apiPath + '/events/' + evand.eventSlug + '/discounts/' + discountCode + '/check';
};
config.updateAttendeeUrl = function (order, attendee) {
  return config.apiPath + '/orders/' + order.id + '/attendees/' + attendee.id;
};
config.getOrderUrl = function (order) {
  return config.apiPath + '/events/' + evand.eventSlug + '/orders/' + order.id + '/?include=attendees,attendees.ticket,attendees.ticket.questions,event,event.questions';
};
config.transactionUrl = function (order) {
  return config.apiPath + '/orders/' + order.id + '/transactions';
};
module.exports = config;
