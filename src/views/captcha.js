var $ = require('mquery');
var xhr = require('xhr');
var config = require('config');

module.exports = function () {
  var container = $
    .create('div')
    .css({
      borderTop: '1px solid #f1f1f1',
      paddingTop: '10px',
      marginTop: '30px'
    })
    .addClass('evand-form-group form-group');

  var key, input;

  function loadCaptcha(event) {
    if (event) {
      event.preventDefault();
    }
    xhr.get(config.captchaUrl, function (err, response, data) {
      data = JSON.parse(data);
      key = data.key;
      container.html('کد کپچا را وارد کنید <br/>');
      input = $
        .create('input')
        .attr('type', 'text')
        .css({width: '100px', display: 'block', margin: '10px 0 10px 10px', float: 'right'})
        .appendTo(container);
      var img = $
        .create('img')
        .css({
          float: 'right',
          display: 'block',
          marginTop: '10px',
          height: '32px'
        })
        .attr('src', data.url)
        .appendTo(container);

      $
        .create('a')
        .attr('href', '#')
        .css({
          float: 'right',
          display: 'block',
          lineHeight: '52px',
          marginRight: '10px',
          fontSize: '.8em'
        })
        .html('کپچای جدید')
        .on('click', loadCaptcha)
        .appendTo(container);
    });
  }

  loadCaptcha();

  return {
    element: container,
    getKey: function () {
      return key;
    },
    getValue: function () {
      return input.val();
    }
  };
};
