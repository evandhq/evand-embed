var $ = require('mquery');
var input = require('./input');

module.exports = function (attendee, questions) {
  var prefix = 'v[' + attendee.id + ']';

  var fields = [
    input({name: prefix + 'first_name', type: 'text', label: 'نام', validate: ['required']}),
    input({name: prefix + 'last_name', type: 'text', label: 'نام خانوادگی', validate: ['required']}),
    input({name: prefix + 'email', type: 'text', label: 'ایمیل', validate: ['required', 'email'], ltr: true}),
    input({name: prefix + 'mobile', type: 'text', label: 'شماره موبایل', validate: ['required', 'mobile'], ltr: true})
  ];

  var wrapper = $.create('div');

  for (var i = 0; i < questions.length; i++) {
    var question = questions[i];
    var validate = [];
    if (question.required === 'yes') {
      validate.push('required');
    }

    fields.push(input({
      name: prefix + 'answers[' + question.id + ']',
      label: question.title,
      type: question.type,
      options: question.options,
      validate: validate
    }));
  }

  for (var j = 0; j < fields.length; j++) {
    wrapper.append(fields[j].element);
  }

  return {
    element: wrapper,
    getValues: function () {
      var values = {};
      for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var key = field.name.replace(prefix, '');
        if (/answers/.test(key)) {
          if (!values.answers) {
            values.answers = {}
          }
          var type = field.element.items[0].childNodes[1].type;
          values.answers[key.replace('answers[', '').replace(']', '')] = {value: field.getValue(), type: type};
        } else {
          values[key] = field.getValue();
        }
      }
      return values;
    },
    validate: function () {
      for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        if (!field.validate()) {
          field.element.find('input').first().focus();
          return false;
        }
      }

      return true;
    }
  };
};
