var $ = require('mquery');
var msg = require('msg');
var persian = require('persian');
var money = persian.money;
var persianNumber = persian.persianNumber;

/**
 * It's the table header :)
 *
 * @type {string}
 */
var tableHeaderRow = '<tr>' +
  '<th >' + msg.row + '</th>' +
  '<th >' + msg.ticketTitle + '</th>' +
  '<th style="width: 100px">' + msg.price + '</th>' +
  '</tr>';

module.exports = function (order) {
  var table = $.create('table').addClass('proforma').css({width: '100%'});
  var thead = $.create('thead').html(tableHeaderRow).appendTo(table);
  var tbody = $.create('tbody').appendTo(table);

  var attendees = order.attendees.data;
  order.price = 0;

  for (var i = 0; i < attendees.length; i++) {
    var attendee = attendees[i];
    $
      .create('tr')
      .append($.create('td').html(persianNumber(i + 1)))
      .append($.create('td').html(attendee.ticket.data.title))
      .append($.create('td').html(money(attendee.price)))
      .appendTo(tbody);

    order.price += attendee.price;
  }

  $
    .create('tr')
    .append($.create('td').css({textAlign: 'left'}).attr('colspan', '2').html(msg.total))
    .append($.create('th').css({textAlign: 'center'}).html(money(order.price)))
    .appendTo(tbody);

  return table;
};
