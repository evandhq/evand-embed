var $ = require('mquery');

var element = $
  .create('div')
  .addClass('loading')
  .html(
    '<svg version="1.1" id="Evanding" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">' +
    '<rect x="0" y="0" width="4" height="10" fill="#333" transform="translate(0 4.88496)">' +
    '<animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0" dur="0.6s" repeatCount="indefinite"></animateTransform>' +
    '</rect>' +
    '<rect x="10" y="0" width="4" height="10" fill="#333" transform="translate(0 18.2183)">' +
    '<animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0.2s" dur="0.6s" repeatCount="indefinite"></animateTransform>' +
    '</rect>' +
    '<rect x="20" y="0" width="4" height="10" fill="#333" transform="translate(0 8.44838)">' +
    '<animateTransform attributeType="xml" attributeName="transform" type="translate" values="0 0; 0 20; 0 0" begin="0.4s" dur="0.6s" repeatCount="indefinite"></animateTransform>' +
    '</rect>' +
    '</svg>'
  );

module.exports = {
  element: element,
  show: function () {
    element.css({display: 'flex'});
  },
  hide: function () {
    element.css({display: 'none'});
  }
};