var $ = require('mquery');
var xhr = require('xhr');
var msg = require('msg');
var config = require('config');
var wrapper = require('wrapper');
var loading = require('loading');
var proforma = require('./proforma');
var notification = require('notification');
var attendeeForm = require('./attendee-form');
var deep = require('../utils/getNestedProperties');
var captcha;

var total = 0, cur = 0;
var hasServerError = false;
var order;
var onComplete = function (error, response) {
  cur += 1;
  if (!(response.statusCode >= 200 && response.statusCode < 300)) {
    hasServerError = true;
    loading.hide();
    notification('خطا در سرور!‌ لطفا داده های خود را چک کنید یا بعدا تلاش فرماید.');
  }

  if (!hasServerError && cur === total) {
    var person = order.attendees.data[0].form.getValues();

    var data = {
      first_name: person.first_name,
      last_name: person.last_name,
      mobile: person.mobile,
      email: person.email,
      gateway: 'zaringate'
    };

    if (captcha) {
      data.captcha = captcha.getValue();
      data.captcha_token = captcha.getKey();
    }

    xhr.post(config.transactionUrl(order), {json: data}, function (err, response, body) {
      if (response.statusCode > 200 && response.statusCode < 300) {
        window.location.href = body.data.gateway_url;
      } else {
        loading.hide();
        notification(body.message);
      }
    });
  }

  if (cur === total) {
    hasServerError = false;
    cur = 0;
    total = 0;
  }
};

var generateFormData = function (values) {
  var formData = new FormData();
  for (var key in values) {
    if (key == 'answers') {
      for (var id in values['answers']) {
        formData.append('answers[' + id + '][type]', values['answers'][id].type);
        formData.append('answers[' + id + '][value]', values['answers'][id].value);
      }
    } else {
      formData.append(key, values[key]);
    }
  }

  return formData;
};

var handleSubmit = function (comingOrder, attendees, event) {
  order = comingOrder;
  event.preventDefault();
  var attendee, valid, values;

  for (var i = 0; i < attendees.length; i++) {
    attendee = attendees[i];

    valid = attendee.form.validate();
    if (!valid) {
      return;
    }
  }

  loading.show();

  total = attendees.length;
  cur = 0;

  for (var k = 0; k < attendees.length; k++) {
    attendee = attendees[k];
    values = attendee.form.getValues();
    xhr.post(config.updateAttendeeUrl(order, attendee), {body: generateFormData(values)}, onComplete);
  }
};


module.exports = function (order) {
  loading.show();

  xhr.get(config.getOrderUrl(order), function (err, response, body) {
    loading.hide();
    order = JSON.parse(body).data;
    wrapper.html('').append(proforma(order));

    var event = order.event.data;
    var attendees = order.attendees.data;
    for (var index in attendees) {
      attendees[index].questions = deep(attendees[index], 'ticket.data.questions.data', []);
    }

    var form = $.create('form').on('submit', handleSubmit.bind(null, order, attendees));

    for (var i = 0; i < attendees.length; i++) {
      var attendee = attendees[i];
      attendee.form = attendeeForm(attendee, attendee.questions);

      $.create('h3').html(msg.ticketHeading(i + 1, attendee)).appendTo(form);
      attendee.form.element.appendTo(form);
    }

    // Captcha
    if (order.require_captcha) {
      captcha = require('./captcha')();
      form.append(captcha.element);
    }

    // Button
    $
      .create('div')
      .addClass('button-wrapper')
      .append($.create('button').html(order.price === 0 ? msg.confirm : msg.confirmAndPay).attr('type', 'submit'))
      .appendTo(form);

    wrapper.append(form);
  });
};