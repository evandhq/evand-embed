var $ = require('mquery');
var validators = require('./../utils/validator');
var notification = require('notification');

module.exports = function (options) {
  var i, option;
  var wrapper = $.create('div').addClass('evand-form-group form-group').css({paddingBottom: '5px'});

  $.create('label').addClass('label-control').appendTo(wrapper).html(options.label);

  var input, getValue;

  switch (options.type) {
    case "textarea":
      input = $.create('textarea').attr('type', 'text').attr('name', options.name).addClass('form-control').appendTo(wrapper);
      break;
    case "select":
      input = $.create('select').attr('name', options.name).addClass('form-control').appendTo(wrapper);
      if (options.options) {
        $
          .create('option')
          .attr('value', '')
          .attr('disabled', 'disabled')
          .attr('selected', 'selected')
          .html('- انتخاب کنید -')
          .appendTo(input);

        for (i = 0; i < options.options.length; i++) {
          option = options.options[i];
          $.create('option').attr('value', option).html(option).appendTo(input);
        }
      }
      break;
    case "checkbox":
    case "radio":
      var inputs = [], inputWrapper = $.create('div').css({display: 'flex'}).appendTo(wrapper);

      if (options.options) {
        for (i = 0; i < options.options.length; i++) {
          option = options.options[i];

          var label = $
            .create('label')
            .css({flexBasis: '25%', flexWrap: 'nowrap'})
            .html(option + ' ')
            .appendTo(inputWrapper);

          inputs.push(
            $
              .create('input')
              .attr('name', options.name)
              .attr('type', options.type)
              .attr('value', option)
              .appendTo(label)
          );
        }
      }

      getValue = function () {
        var value = [];
        for (var j = 0; j < inputs.length; j++) {
          var input = inputs[j];
          if (input.first().checked) {
            value.push(input.val());
          }
        }

        return value.join(',');
      };
      break;
    case "file":
      input = $
        .create('input')
        .attr('type', 'file')
        .attr('name', options.name)
        .addClass('form-control')
        .appendTo(wrapper);

      getValue = function () {
        return input.items[0].files[0];
      };
      break;
    case "text":
    default:
      input = $
        .create('input')
        .attr('type', 'text')
        .attr('name', options.name)
        .addClass('form-control')
        .appendTo(wrapper);

      if (options.ltr) {
        input.attr('dir', 'ltr')
      }
  }

  if (!getValue) {
    getValue = function () {
      return input.val();
    };
  }

  var validate = function () {
    var value = getValue();
    for (var i = 0; i < options.validate.length; i++) {
      var validator = validators[options.validate[i]];
      var message = validator(value, options.label);
      if (message) {
        notification(message);
        return false;
      }
    }
    return true;
  };

  return {
    element: wrapper,
    getValue: getValue,
    name: options.name,
    validate: validate
  };
};