var $ = require('mquery');

var element = $
  .create('div')
  .addClass('notify');

var timeout = 0;

module.exports = function (message, time) {
  element.addClass('active').html(message);
  window.clearTimeout(timeout);
  timeout = window.setTimeout(function () {
    element.removeClass('active');
  }, time || 2000);
};

module.exports.element = element;
