var $ = require('mquery');
var config = require('config');
var wrapper = require('wrapper');
var notification = require('notification');
var xhr = require('xhr');
var msg = require('msg');
var loading = require('loading');
var persian = require('persian');
var informationForm = require('./information-form');
var parseDate = require('parseDate');
var persianNumber = persian.persianNumber;
var money = persian.money;
var formatDate = persian.formatDate;

/**
 * It's the table header :)
 *
 * @type {string}
 */

var tableHeaderRow = '<tr>' +
  '<th style="text-align: right">' + msg.ticketTitle + '</th>' +
  '<th>' + msg.deadLine + '</th>' +
  '<th>' + msg.price + '</th>' +
  '<th style="width: 60px">' + msg.count + '</th>' +
  '</tr>';

/**
 * It returns the select options according to the given ticket
 *
 * @param ticket
 * @returns {*}
 */
var renderQuantityOption = function (ticket) {
  return Array(Math.min(ticket.available_count, typeof ticket.max_count === 'number' ? ticket.max_count : 5) + 1)
    .fill()
    .map(function (_, i) {
      return i;
    })
    // Min
    .filter(function (_, i) {
      return i >= Math.max(typeof ticket.min_count === 'number' ? ticket.min_count : 1);
    })
    .map(function (value) {
      return $.create('option').attr('value', value).html(persianNumber(value));
    });
};

var sendOrder = function (data) {
  xhr.post(config.orderUrl, {json: data}, function (err, response, body) {
    loading.hide();

    if (response.statusCode > 400 && response.statusCode < 500) {
      if (body.errors) {
        for (var ticket in body.errors) {
          return notification(body.errors[ticket]);
        }
      } else {
        notification(body.message);
      }

    } else if (response.statusCode > 200 && response.statusCode < 300) {
      var order = body.data;
      informationForm(order);
    }
  });
};

/**
 * Handle submit event
 *
 * @param e
 */
var handleSubmit = function (tickets, e) {
  e.preventDefault();
  var data = {
    tickets: []
  };

  $('.count-input').each(function () {
    var count = parseInt($(this).val());
    if (!isNaN(count) && count > 0) {
      var ticketId = $(this).attr('data-ticket');
      var object = {
        id: ticketId,
        count: $(this).val()
      };

      var priceElement = $('.price-input[data-ticket="' + ticketId + '"]');
      if (priceElement.items.length) {
        object.price = priceElement.val();
      }

      data.tickets.push(object);
    }
  });

  if (data.tickets.length === 0) {
    return notification('باید حداقل یک بلیت انتخاب کنید.');
  }

  var discount;
  var discountElement = $('#discount');

  if (discountElement.items.length > 0) {
    discount = discountElement.val().trim();
  }

  if (discount) {
    data.discount_code = discount;
  }

  loading.show();
  if (discount) {
    xhr.get(config.discountCheckUrl(discount), function (err, response, body) {
      if (response.statusCode == 200) {
        sendOrder(data);
      } else {
        notification(msg.invalidDiscount);
        loading.hide();
      }
    });
  } else {
    sendOrder(data);
  }
};

var renderPrice = function (ticket) {
  var priceElement;

  if (ticket.type === 'donation') {
    priceElement = $
      .create('input')
      .addClass('price-input')
      .attr('type', 'text')
      .css({width: '125px'})
      .attr('data-ticket', ticket.id)
      .attr('placeholder', 'مبلغ حمایت (تومان)');
  } else {
    priceElement = $.create('span').html(money(ticket.price));
  }

  return priceElement;
};

var renderCount = function (event, ticket, selectMinimum) {
  var countInput;
  if (event.ended) {
    countInput = $.create('span').html(msg.soldOut);
  } else {
    var options = renderQuantityOption(ticket);

    if (options.length > 0) {
      countInput = $
        .create('select')
        .attr('data-ticket', ticket.id)
        .addClass('count-input');

      if (!selectMinimum) {
        countInput.append($.create('option').attr('value', '').html('-'));
      }

      countInput.append(renderQuantityOption(ticket)).addClass('count');
    } else {
      countInput = $.create('span').html('ظرفیت تمام شد');
    }
  }

  return countInput;
};

/**
 * Render the form when event is loaded
 *
 * @param event
 */
var renderForm = function (event) {
  var tickets = event.tickets.data;
  var form = $.create('form').attr('action', '#').on('submit', handleSubmit.bind(null, tickets));
  var table = $.create('table').addClass('order').css({width: '100%'}).appendTo(form);
  var thead = $.create('thead').html(tableHeaderRow).appendTo(table);
  var tbody = $.create('tbody').appendTo(table);

  if(tickets.length > 0) {
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      var date = formatDate(parseDate(ticket.end_date || event.end_date), '%Y/%M/%D');
      var title;
      if (ticket.description) {
        title = $.create('td')
          .append($.create('div').html(ticket.title))
          .append($.create('small').html(ticket.description));

      } else {
        title = $.create('td').html(ticket.title)
      }
      var tr = $
        .create('tr')
        .append(title)
        .append($.create('td').html(date))
        .append($.create('td').append(renderPrice(ticket)).addClass('evand-form-group'))
        .append($.create('td').append(renderCount(event, ticket, tickets.length === 1)))
        .appendTo(tbody);
    }
  } else {
    var tr = $
      .create('tr')
      .append($.create('td').attr('colspan', '4').css({'text-align':'center'}).html('ظرفیت این رویداد پر شده است.'))
      .appendTo(tbody);
  }
  // Discount Code
  var showDiscount = false;
  if (!(event.ended || event.soldOut)) {
    showDiscount = event.tickets.data.some(function (item) {
      return item.type === 'normal' && item.price > 0;
    });
  }
  if (showDiscount) {
    $
      .create('div')
      .addClass('evand-form-group')
      .addClass('discount-wrapper')
      .css({
        float: 'left',
        width: '200px',
        textAlign: 'left',
        padding: '10px 5px'
      })
      .append($.create('input').attr('id', 'discount').attr('type', 'text').attr('placeholder', 'کد تخفیف'))
      .appendTo(form);
  }

  // Button
  $
    .create('div')
    .addClass('button-wrapper')
    .append($.create('button').html('خرید').attr('type', 'submit'))
    .appendTo(form);

  wrapper.append(form);
};

module.exports = function () {
  loading.show();

  xhr.get(config.eventUrl, function (err, response, body) {
    loading.hide();

    var event = JSON.parse(body).data;

    renderForm(event);
  });
};
