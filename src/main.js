// Apply pollyfills
require('utils/polyfills');

// Apply the styles
require('./styles/' + (evand.theme || 'default') + '.scss');

// Create the wrapper
require('wrapper');

require('views/order-init')();
