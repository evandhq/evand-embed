var latinToPersianMap = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰'];
var latinNumbers = [/1/g, /2/g, /3/g, /4/g, /5/g, /6/g, /7/g, /8/g, /9/g, /0/g];
var jalali = require('jalaali-js');

function latinToPersian(string) {
  var result = string;

  for (var index = 0; index < 10; index++) {
    result = result.replace(latinNumbers[index], latinToPersianMap[index]);
  }

  return result;
}

function money(price) {
  var string = typeof price !== 'string' ? price.toString() : price;
  return string === '0' ? 'رایگان' : persianNumber(string.replace(/\B(?=(\d{3})+\b)/g, ','));
}

function persianNumber(input) {
  var string = typeof input === 'number' ? input.toString() : input;
  return latinToPersian(string);
}

function formatDate(date, format) {
  date = jalali.toJalaali(date.getFullYear(), date.getMonth() + 1, date.getDate());
  return persianNumber(format.replace('%Y', date.jy).replace('%M', date.jm).replace('%D', date.jd));
}

module.exports = {
  persianNumber: persianNumber,
  money: money,
  formatDate: formatDate
};
