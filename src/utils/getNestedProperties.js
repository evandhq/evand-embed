module.exports = function (baseObject, dotNotation, defaultProperty) {
  const notations = dotNotation.split('.');
  var temporaryObject = baseObject;

  for (var index in notations) {
    temporaryObject = temporaryObject[notations[index]];
    if (temporaryObject === undefined)
      return defaultProperty;
  }

  return temporaryObject;
};
