var isEmpty = function (value) {
  return typeof value === 'undefined' || value === null || value === '';
};

var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
var mobileRegex = /^(\+989|09)[0-3][0-9][0-9]{7}$/;

exports.required = function (value, label) {
  if (isEmpty(value)) {
    return 'وارد کردن ' + label + ' الزامی است.';
  }
};

exports.mobile = function (value) {
  if (!isEmpty(value) && !mobileRegex.test(value)) {
    return 'شماره موبایل وارد شده نامعتبر است.';
  }
};

exports.email = function (value) {
  if (!isEmpty(value) && !emailRegex.test(value)) {
    return 'ایمیل وارد شده نامعتبر است.';
  }
};
