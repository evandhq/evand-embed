module.exports = function parseDate(string) {
  // For IE support
  var date = string.match(/^(\d{4})\-(\d\d)\-(\d\d)/);
  return new Date(date[1], date[2] - 1, date[3]);
};
