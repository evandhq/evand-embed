var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: 'main.js',
  resolve: {
    alias: {
      parseDate: 'utils/parseDate.js',
      persian: 'utils/persian.js',
      styles: 'utils/styles.js',
      mquery: 'utils/mquery.js',
      config: 'config.js',
      wrapper: 'wrapper.js',
      loading: 'views/loading.js',
      notification: 'views/notification.js',
      msg: 'messages.js'
    },
    modulesDirectories: [
      'src',
      'node_modules'
    ]
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: 'style!css!sass'
      },
      {
        test: /\.png$/,
        loader: 'url-loader?limit=10240'
      }
    ]
  },
  output: {
    filename: 'main.js',
    path: __dirname + '/dist'
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ]
};
